from debian:bookworm-slim

run DEBIAN_FRONTEND=noninteractive apt-get update
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt-utils
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  locales

RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

run DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

run DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
  procps \
  cron \
  curl \
  sudo \
  memcached

COPY ./entrypoint.sh /root/entrypoint.sh
RUN chmod 700 /root/entrypoint.sh
ENTRYPOINT [ "/root/entrypoint.sh" ]

