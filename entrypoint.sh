#!/usr/bin/env bash

chown www-data:crontab /var/spool/cron/crontabs/www-data \
  && chmod 600 /var/spool/cron/crontabs/www-data

/usr/sbin/cron

/usr/bin/sudo -u memcache /usr/bin/memcached
#tail -f /dev/null

